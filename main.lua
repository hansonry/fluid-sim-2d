
local ColorPercent = require("ColorPercent")

local Material = {}
Material.__index = Material

function Material:new(name, color)
   local mat = {
      name = name,
      color = color,
   }
   setmetatable(mat, self)
   return mat
end

local materials = (function(mats)
   local list = {}
   for i, v in ipairs(mats) do
      list[v.name] = v
   end
   return list
end)({
   Material:new("O2",  {0.5, 0,   0}),
   Material:new("CO2", {0,   0.5, 0}),
   Material:new("N2",  {0,   0,   0.5}),
})

function getMaterialObj(mat)
   if type(mat) == "string" then
      local matObj = materials[mat]
      assert(mat, "Unknown material " .. mat)
      return matObj
   end
   return mat
end


local MaterialSet = {}
MaterialSet.__index = MaterialSet


function MaterialSet:new()
   local matSet = {
      contents = {},
      temperatureK = 294.3
   }
   setmetatable(matSet, self)
   return matSet
end

function MaterialSet:makeOrClear(matSet)
   if matSet == nil then
      return self:new()
   end
   matSet:clear()
   return matSet
end

function MaterialSet:_getMaterialMols(matObj)
   if self.contents[matObj] == nil then
      return 0
   end
   return self.contents[matObj]
end

function MaterialSet:_clearMaterialMols(matObj)
   self.contents[matObj] = nil 
end

function MaterialSet:clearMaterialMols(mat)
   local matObj = getMaterialObj(mat)
   self:_clearMaterialMols(matObj)
end

function MaterialSet:clear()
   for k in next, self.contents do
      self.contents[k] = nil
   end
end

function MaterialSet:_setMaterialMols(matObj, amountInMols)
   assert(amountInMols >= 0, "amountInMols should be positive or zero")
   if amountInMols == 0 then 
      self.contents[matObj] = nil 
   else
      self.contents[matObj] = amountInMols
   end
end


function MaterialSet:setMaterialMols(mat, amountInMols)
   local matObj = getMaterialObj(mat)
   self:_setMaterialMols(matObj, amountInMols)
end

function MaterialSet:setToFractionOfMaterialSet(matSet, fraction)
   self:clear()
   for matObj, amountInMols in pairs(matSet.contents) do
      self:_setMaterialMols(matObj, amountInMols * fraction)
   end
end


function MaterialSet:getMaterialMols(mat)
   local matObj = getMaterialObj(mat)
   return self:_getMaterialMols(matObj)
end

function MaterialSet:_addMaterialMols(matObj, amountInMols)
   local currentInMols = self:_getMaterialMols(matObj)
   self:_setMaterialMols(matObj, currentInMols + amountInMols)
end

function MaterialSet:addMaterialMols(mat, amountInMols)
   local matObj = getMaterialObj(mat)
   self:_addMaterialMols(matObj, amountInMols)
end

function MaterialSet:addMaterialSet(matSet)
   for matObj, amountInMols in pairs(matSet.contents) do
      self:_addMaterialMols(matObj, amountInMols)
   end
end

function MaterialSet:computeTotalMols()
   local totalMols = 0
   local isVacuum = true
   for matObj, amountInMols in pairs(self.contents) do
      totalMols = totalMols + amountInMols
      isVacuum = false
   end
   return totalMols, isVacuum
end

function MaterialSet:computePercents(totalMols, isVacuum)
   local percents = {}
   if totalMols == nil or isVacuum == nil then
      totalMols, isVacuum = self:computeTotalMols()
   end
   if isVacuum then
      return percents
   end
   for matObj, amountInMols in pairs(self.contents) do
      percents[matObj] = amountInMols / totalMols
   end
   
   return percents
end


local Link = {}
Link.__index = Link

function Link:new(cellSrc, cellDest, dx, dy)
   function computeLength()
      return math.sqrt(dx * dx + dy * dy)
   end
   local link = {
      cellSrc = cellSrc,
      cellDest = cellDest,
      dx = dx,
      dy = dy,
      length = computeLength(),
      matSetSrc = MaterialSet:new(),
      matSetDest = MaterialSet:new()
   }
   setmetatable(link, self)
   return link
end

function Link:swapMatSet()
   local temp = self.matSetSrc
   self.matSetSrc = self.matSetDest
   self.matSetDest = temp
end

function Link:getMatSetAtCell(cell)
   if cell == self.cellSrc then
      return self.matSetSrc
   elseif cell == self.cellDest then
      return self.matSetDest
   else
      assert(false, "cell not in link")
   end
end


local Cell = {gasConstant = 8.314}
Cell.__index = Cell

function Cell:new()
   local cell = {
      links = {},
      weights = {},
      matSet = MaterialSet:new(),
      temperatureK = 294.3,
      volumeM3 = 1
   }
   setmetatable(cell, self)
   return cell
end


function Cell:addLink(link)
   table.insert(self.links, link)
 end

function Cell:computeWeights()
   local easeList = {}
   local easeSum = 0
   local centerWeight = 0.3 -- Center (0 Velocity) Weight
   self.weights[1] = centerWeight 
   for i, link in ipairs(self.links) do
      local ease = 1 / link.length 
      easeSum = easeSum + ease
      easeList[i] = ease
   end
   
   local centerSuck = centerWeight / #self.links
   for i, ease in ipairs(easeList) do
      self.weights[i + 1] = (ease / easeSum) - centerSuck
   end
end

function Cell:_sumUpLinks(matSet)
   matSet = MaterialSet:makeOrClear(matSet)
   matSet:addMaterialSet(self.matSet)
   for i, link in ipairs(self.links) do
      local linkMatSet = link:getMatSetAtCell(self)
      matSet:addMaterialSet(linkMatSet)
   end
   return matSet
end

function Cell:computeCellData(matSet)
   matSet = self:_sumUpLinks(matSet)
   local totalMols, isVacuum = matSet:computeTotalMols()
   local percents = matSet:computePercents(totalMols, isVacuum)
   return {
      isVacuum = isVacuum,
      totalMols = totalMols,
      percents = percents,
      pressure = totalMols * self.gasConstant * self.temperatureK / self.volumeM3
   }
end

function Cell:resolveCollision(matSet)
   matSet = self:_sumUpLinks(matSet)
   local totalMols, isVacuum = matSet:computeTotalMols()
   if not isVacuum then
      self.matSet:setToFractionOfMaterialSet(matSet, self.weights[1])
      for i, link in ipairs(self.links) do
         local linkMatSet = link:getMatSetAtCell(self)
         linkMatSet:setToFractionOfMaterialSet(matSet, self.weights[i + 1])
      end
   end
end


local Network = {}
Network.__index = Network

function Network:new(width, height)
   local net = {
      width = width,
      height = height,
      maxCellIndex = height * width + 1,
      cells = {},
      links = {}
   }
   setmetatable(net, self)
   function setupNetwork()
      local cellIndex = 1;
      for y = 1, net.height do
         for x = 1, net.width do
            local cell = Cell:new()
            net.cells[cellIndex] = cell
            function addLinkTo(destX, destY)
               local cellDest = net:getCell(destX, destY)
               assert(cellDest, string.format("Expected to find a cell at %d, %d", destX, destY))
               local dx = destX - x
               local dy = destY - y
               local link = Link:new(cell, cellDest, dx, dy)
               cell:addLink(link)
               cellDest:addLink(link)
               net:addLink(link)
            end
            if y > 1 then
               addLinkTo(x, y - 1)
            end
            if x > 1 then
               addLinkTo(x - 1, y)
            end
            if y > 1 and x > 1 then
               addLinkTo(x - 1, y - 1)
            end
            if y > 1 and x < net.width then
               addLinkTo(x + 1, y - 1)
            end

            cellIndex = cellIndex + 1
         end
      end
      -- Now that everything is created and connected.
      -- Compute weights on all the cells
      for i, cell in ipairs(net.cells) do
         cell:computeWeights()
      end
   end
   setupNetwork()
   return net
end


function Network:addLink(link)
   table.insert(self.links, link)
end

function Network:getCellIndex(x, y)
   return (y - 1) * self.width + x
end

function Network.isCellIndexInRange(index)
   return index < 1 or index > self.maxCellIndex 
end

function Network:isCellCoordInRange(x, y)
   return (x >= 1 and x <= self.width and 
           y >= 1 and y <= self.height)
end

function Network:getCell(x, y, otherwise)
   if self:isCellCoordInRange(x, y) then
      local cellIndex = self:getCellIndex(x, y)
      return self.cells[cellIndex]
   end
   return otherwise
end

function Network:_swapVelocities()
   for i, link in ipairs(self.links) do
      link:swapMatSet()
   end
end

function Network:tick()
   print("tick")
   
   -- Step one: Resolve collision 
   local tempMem = MaterialSet:new()
   for i, cell in ipairs(self.cells) do
      cell:resolveCollision(tempMem)
   end
   self:_swapVelocities()
end

function Network:computeTotalMols()
   local totalMols = 0
   for i, cell in ipairs(self.cells) do
      totalMols = totalMols + cell.matSet:computeTotalMols()
   end
   for i, link in ipairs(self.links) do
      totalMols = totalMols + link.matSetSrc:computeTotalMols()
      totalMols = totalMols + link.matSetDest:computeTotalMols()
   end
   return totalMols
end

local net = Network:new(10, 10)


function love.load()
   local cell1 = net:getCell(1, 1)
   cell1.matSet:addMaterialMols("O2", 9.36)
   local cell2 = net:getCell(10, 10)
   cell2.matSet:addMaterialMols("N2", 15)
   print("Load")
end

local function kelvinToCelsius(kelvin)
   return kelvin - 273.15
end

function drawNetwork(net, xStart, yStart, cellSizePx)
   local g = love.graphics
   cellSizePx = cellSizePx or 16
   xStart = xStart or 0
   yStart = yStart or 0
   
   love.graphics.translate(.5, .5) -- Apparently to get clean pixels you need to do this
   
   local maxMol = 20
   
   function drawCellInternal(cell, x, y, w, h)
      local halfW = w / 2
      local halfH = h / 2
      local cellData = cell:computeCellData()
      if not cellData.isVacuum then
         -- Draw Bars in sorted order by percentage
         local sortedMat = {}
         for objMat, percent in pairs(cellData.percents) do
            table.insert(sortedMat, {percent = percent, objMat = objMat})
         end
         table.sort(sortedMat, function(a, b) return a.percent < b.percent end)
         local yOffset = 0
         for i, v in ipairs(sortedMat) do
            local pHeight = h * v.percent
            g.setColor(v.objMat.color)
            g.rectangle("fill", x, y + yOffset, halfW, pHeight)
            yOffset = yOffset + pHeight
         end
         
         local molPercent = cellData.totalMols / maxMol
         g.setColor(ColorPercent(molPercent))
         g.rectangle("fill", x + halfW, y, halfW, halfH)

      end
   end
   
   for y = 1, net.height do
      for x = 1, net.width do
         local px = (x - 1) * cellSizePx
         local py = (y - 1) * cellSizePx
         
         local cell = net:getCell(x, y)
         drawCellInternal(cell, px + 1, py + 1, cellSizePx - 1, cellSizePx - 1)
         
         g.setColor(.6, .6, .6)
         g.rectangle("line", px, py, cellSizePx, cellSizePx)
         
      end
   end
   
   love.graphics.translate(-.5, -.5) -- Fix the Changes
   
   local mouseX,mouseY = love.mouse.getPosition()
   
   local cellX = math.floor((mouseX - xStart) / cellSizePx) + 1
   local cellY = math.floor((mouseY - yStart) / cellSizePx) + 1
   
   local cell = net:getCell(cellX, cellY)
   if cell ~= nil then
      local cellData = cell:computeCellData()
      local printStr = string.format("Cell (%d, %d)", cellX, cellY)
      if not cellData.isVacuum then
      printStr = (printStr .. 
         string.format("\nMols %.8f\nPressure %.8f(Pa)\nTemperature %.8f(K) %.2f(C)", 
                       cellData.totalMols, cellData.pressure, 
                       cell.temperatureK, 
                       kelvinToCelsius(cell.temperatureK)))

      end
      g.setColor(1, 1, 1)
      g.print(printStr, mouseX, mouseY + 20)
   end
   

end

local simulationState = "paused"

local timeout = 0.5
local timer = 0


function love.draw()
   drawNetwork(net)
   local totalMols = net:computeTotalMols()
   love.graphics.setColor(1, 1, 1)
   love.graphics.print(string.format("Simulation State: %s\nTick Speed %f Seconds\n Total Mols: %.5f", simulationState, timeout, totalMols), 400, 300)
end

function love.keypressed(key)
   if key == "space" then
      if simulationState == "paused" then
         simulationState = "play"
      else
         simulationState = "paused"
      end
   elseif key == "n" then
      net:tick()
   elseif (key == "1" or
           key == "2" or 
           key == "3" or
           key == "4" or
           key == "5" or
           key == "6" or
           key == "7" or
           key == "8" or
           key == "9") then
      local num = tonumber(key)
      timeout = 1 / num
   elseif key == "0" then
      timeout = 0
   end
   
end




function love.update(dt)
   if simulationState == "play" then
      timer = timer + dt
      if timer >= timeout then
         net:tick()
         timer = timer - timeout
      end
   else
      timer = 0
   end
end